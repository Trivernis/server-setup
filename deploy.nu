#!/bin/env nu

export  def main [] {
  let last_rev = git rev-parse HEAD
  git fetch origin
  git reset --hard origin/main
  git submodule deinit -f .
  git submodule update --init --recursive --checkout
  git-crypt unlock ../cfg-secret

  let changeset = get_changed_dirs $last_rev
  print $"changeset: ($changeset | str join ', ')"

  if not ("./data" | path exists) {
    mkdir "./data"
  }

  ( $changeset
   | where $it != "traefik"
   | par-each { 
      try {
        deploy_compose $in
      } catch {|e| 
        print -e $e.msg
      }
    }
    | ignore
  )

  if "traefik" in $changeset {
    deploy_compose ./traefik/
  }
}

def deploy_compose [path: string] {
  print $"applying ($path)"
  cd $path

  if ("./build.nu" | path exists) {
    ./build.nu
  } else {
    docker-compose -f compose.yml pull
    docker-compose -f compose.yml build --pull
  }

  if ("./start.nu" | path exists) {
    ./start.nu
  } else {
    docker-compose -f compose.yml up -d --force-recreate --wait --remove-orphans
  }
  
  print $"($path) applied"
}

def get_changed_dirs [rev: string] {
  ( git diff $rev --name-only 
    | lines
    | each { path parse | get parent }
    | where $it != ""
    | where {|p| $p | path join "compose.yml" | path exists }
    | uniq
  )
}
